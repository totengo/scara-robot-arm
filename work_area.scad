module line(point1, point2, width = 1) {
    angle = 90 - atan((point2[1] - point1[1]) / (point2[0] - point1[0]));
    offset_x = 0.5 * width * cos(angle);
    offset_y = 0.5 * width * sin(angle);

    offset1 = [-offset_x, offset_y];
    offset2 = [offset_x, -offset_y];

    polygon(points=[
        point1 + offset1, point2 + offset1,  
        point2 + offset2, point1 + offset2
    ]);
}
module point(point, thickness) {
    translate(point)circle(thickness);
}
//point([0, 10], 2);
//line([0, 0], [0, 10], 0.001);
l1=150; l2=150;
function gerapos(t1,t2)=[l1*cos(t1)+l2*cos(t1+t2), l1*sin(t1)+l2*sin(t1+t2)];
posicoes=[for(i=[0:10:180],j=[0:10:150]) gerapos(i, j)];
echo(posicoes[3]);
for(i=[0:1:len(posicoes)-1]) {
    //line(posicoes[i], posicoes[i+1], 0.001);
    point(posicoes[i], 3);
}